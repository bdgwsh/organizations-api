#!/bin/sh

source ./template.env


echo "Waiting while database is up at ${DB_HOST}:${DB_PORT}"
while ! nc -z ${DB_HOST} ${DB_PORT};
do
    sleep 0.3;
    echo "Database is not ready! Waiting.."
done;
echo "Database is ready"

cd source
./manage.py makemigrations
./manage.py migrate
echo "Collecting static.."
./manage.py collectstatic --noinput
echo "Collecting static completed"
gunicorn -b 0.0.0.0:${APP_PORT} core.wsgi


