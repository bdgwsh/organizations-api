from django.urls import reverse
from rest_framework.test import APITestCase

from organizations.models import Organization, OrganizationsNetwork, District
from products.models import ProductPrice, Product, Category


class OrganizationsTest(APITestCase):

    def setUp(self):
        self.magnit_organization_network = OrganizationsNetwork.objects.create(
            title='Magnit'
        )

        self.north_district = District.objects.create(
            title='Северный'
        )
        self.south_district = District.objects.create(
            title='Южный'
        )
        self.west_district = District.objects.create(
            title='Западный'
        )
        self.east_district = District.objects.create(
            title='Восточный'
        )

        self.magnit_at_home_organization = Organization.objects.create(
            title='Магнит у дома',
            description='Домашний магнит',
            organization_network=self.magnit_organization_network
        )
        self.magnit_at_home_organization.district.add(self.east_district)
        self.magnit_at_home_organization.district.add(self.south_district)
        self.magnit_at_home_organization.district.add(self.west_district)

        self.magnit_drugstore_organization = Organization.objects.create(
            title='Магнит-аптека',
            description='Аптека. Медикаменты и лекрства.',
            organization_network=self.magnit_organization_network
        )
        self.magnit_drugstore_organization.district.add(self.east_district)
        self.magnit_drugstore_organization.district.add(self.west_district)

        self.drugstore_category = Category.objects.create(
            title='Биологически активные добавки'
        )

        self.magnit_at_home_hematogen_ = Product.objects.create(
            title='гематоген',
            category=self.drugstore_category
        )

        self.tableware_category = Category.objects.create(
            title='Посуда'
        )

        self.cup = Product.objects.create(
            title='Кружка',
            category=self.tableware_category
        )

        self.cup_ = ProductPrice.objects.create(
            organization=self.magnit_at_home_organization,
            product=self.cup,
            price=230.00,
        )

        self.magnit_at_home_hematogen = ProductPrice.objects.create(
            organization=self.magnit_at_home_organization,
            product=self.magnit_at_home_hematogen_,
            price=30.00,
        )

        self.magnit_drugstore_hematogen_ = Product.objects.create(
            title='гематоген',
            category=self.drugstore_category
        )
        self.magnit_drugstore_hematogen = ProductPrice.objects.create(
            organization=self.magnit_drugstore_organization,
            product=self.magnit_drugstore_hematogen_,
            price=20.00,
        )

    def test_get_organization_info(self):
        url = reverse('organizations:organization-detail', args=[self.magnit_at_home_organization.id])
        response = self.client.get(url)
        self.assertEquals(response.data['title'], self.magnit_at_home_organization.title, response.data)
        self.assertEquals(len(response.data['district']), 3, response.data)
        self.assertEquals(len(response.data['product_prices']), 2, response.data)

    def test_get_detailed_product_price(self):
        url = reverse('products:product_price-detail', args=[self.magnit_at_home_hematogen.id])
        response = self.client.get(url)
        self.assertEquals(response.data['product'], self.magnit_at_home_hematogen.product.title, response.data)
        self.assertEquals(float(response.data['price']), self.magnit_at_home_hematogen.price, response.data)

    def test_get_organizations_by_district_id(self):
        url = reverse('organizations:organization-by-district-list', args=[self.east_district.id])
        response = self.client.get(url)
        self.assertEquals(len(response.data), 2, response.data)
        self.assertEquals(response.data[0]['title'], self.magnit_at_home_organization.title, response.data)
        self.assertEquals(response.data[1]['title'], self.magnit_drugstore_organization.title, response.data)

        url = reverse('organizations:organization-by-district-list', args=[self.south_district.id])
        response = self.client.get(url)
        self.assertEquals(len(response.data), 1, response.data)
        self.assertEquals(response.data[0]['title'], self.magnit_at_home_organization.title, response.data)

    def test_filter_organizations_with_district_id_by_price(self):
        url = reverse('organizations:organization-by-district-list', args=[self.east_district.id])

        query_params = {'max_price': 20.00}
        response = self.client.get(url, query_params)
        self.assertEquals(len(response.data), 1, response.data)
        self.assertEquals(response.data[0]['title'], self.magnit_drugstore_organization.title, response.data)

        query_params = {'max_price': 30.00}
        response = self.client.get(url, query_params)
        self.assertEquals(len(response.data), 2, response.data)
        self.assertEquals(response.data[1]['title'], self.magnit_at_home_organization.title, response.data)
        self.assertEquals(response.data[0]['title'], self.magnit_drugstore_organization.title, response.data)

        query_params = {'min_price': 20.00}
        response = self.client.get(url, query_params)
        self.assertEquals(len(response.data), 2, response.data)
        self.assertEquals(response.data[1]['title'], self.magnit_at_home_organization.title, response.data)
        self.assertEquals(response.data[0]['title'], self.magnit_drugstore_organization.title, response.data)

        query_params = {'min_price': 30.00}
        response = self.client.get(url, query_params)
        self.assertEquals(len(response.data), 1, response.data)
        self.assertEquals(response.data[0]['title'], self.magnit_at_home_organization.title, response.data)

    def test_filter_organizations_with_district_id_by_title(self):
        url = reverse('organizations:organization-by-district-list', args=[self.east_district.id])

        query_params = {'title': 'Магнит'}
        response = self.client.get(url, query_params)
        self.assertEquals(len(response.data), 2, response.data)
        self.assertEquals(response.data[0]['title'], self.magnit_at_home_organization.title, response.data)
        self.assertEquals(response.data[1]['title'], self.magnit_drugstore_organization.title, response.data)

        query_params = {'title': 'у дома'}
        response = self.client.get(url, query_params)
        self.assertEquals(len(response.data), 1, response.data)
        self.assertEquals(response.data[0]['title'], self.magnit_at_home_organization.title, response.data)

    def test_filter_organizations_with_district_id_by_product(self):
        url = reverse('organizations:organization-by-district-list', args=[self.east_district.id])

        query_params = {'product': 'гематоген'}
        response = self.client.get(url, query_params)
        self.assertEquals(len(response.data), 2, response.data)
        self.assertEquals(response.data[0]['title'], self.magnit_at_home_organization.title, response.data)
        self.assertEquals(response.data[1]['title'], self.magnit_drugstore_organization.title, response.data)

        query_params = {'product': 'Кружка'}
        response = self.client.get(url, query_params)
        self.assertEquals(len(response.data), 1, response.data)
        self.assertEquals(response.data[0]['title'], self.magnit_at_home_organization.title, response.data)

    def test_filter_organizations_with_district_id_by_category(self):
        url = reverse('organizations:organization-by-district-list', args=[self.east_district.id])

        query_params = {'category': 'добавки'}
        response = self.client.get(url, query_params)
        self.assertEquals(len(response.data), 2, response.data)
        self.assertEquals(response.data[0]['title'], self.magnit_at_home_organization.title, response.data)
        self.assertEquals(response.data[1]['title'], self.magnit_drugstore_organization.title, response.data)

        query_params = {'category': 'Посуда'}
        response = self.client.get(url, query_params)
        self.assertEquals(len(response.data), 1, response.data)
        self.assertEquals(response.data[0]['title'], self.magnit_at_home_organization.title, response.data)
