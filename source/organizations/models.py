from django.db import models


class District(models.Model):
    title = models.CharField(max_length=255, verbose_name='Наименование района', unique=True)

    def __str__(self):
        return self.title

    def __repr__(self):
        return self.__str__()

    class Meta:
        verbose_name = 'Район'
        verbose_name_plural = 'Районы'


class OrganizationsNetwork(models.Model):
    title = models.CharField(max_length=255, verbose_name='Имя сети', unique=True)

    def __str__(self):
        return self.title

    def __repr__(self):
        return self.__str__()

    class Meta:
        verbose_name = 'Сеть предприятий'
        verbose_name_plural = 'Сети предприятий'


class Organization(models.Model):
    title = models.CharField(max_length=255, verbose_name='Имя предприятия', unique=True)
    description = models.TextField(verbose_name='Описание предприятия')
    organization_network = models.ForeignKey(OrganizationsNetwork,
                                             related_name='organizations', on_delete=models.CASCADE, null=False)
    district = models.ManyToManyField(District, related_name='organizations')

    def __str__(self):
        return self.title

    def __repr__(self):
        return self.__str__()

    class Meta:
        verbose_name = 'Предприятие'
        verbose_name_plural = 'Предприятия'
