from django_filters import rest_framework as filters

from .models import Organization


class OrganizationListFilter(filters.FilterSet):
    title = filters.CharFilter(lookup_expr='contains')
    product = filters.CharFilter(field_name="product_prices__product__title", lookup_expr='contains')
    category = filters.CharFilter(field_name="product_prices__product__category__title", lookup_expr='contains')
    min_price = filters.NumberFilter(field_name="product_prices__price", lookup_expr='gte')
    max_price = filters.NumberFilter(field_name="product_prices__price", lookup_expr='lte')

    class Meta:
        model = Organization
        fields = ['title', 'product', 'category', 'min_price', 'max_price']
