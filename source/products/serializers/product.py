from rest_framework import serializers

from products.models import ProductPrice, Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'title')


class ProductPriceSerializer(serializers.ModelSerializer):
    product = serializers.StringRelatedField()

    class Meta:
        model = ProductPrice
        fields = ('id', 'product', 'price')
