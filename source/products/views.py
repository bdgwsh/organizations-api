from rest_framework import generics
from rest_framework.permissions import AllowAny

from products.models import ProductPrice
from products.serializers.product import ProductPriceSerializer


class ProductPriceDetail(generics.RetrieveAPIView):
    queryset = ProductPrice.objects.all()
    serializer_class = ProductPriceSerializer
    permission_classes = (AllowAny, )
